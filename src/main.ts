import Vue from 'vue'
import App from '@/App.vue'

import router from '@/router'
import store from '@/store'

import '@/sw'

import '@/styles/tailwind.css'
import 'pwacompat'

// Vue Config
Vue.config.productionTip = false

// Initiate Vue Constructor
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
