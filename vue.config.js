const ReplaceInFilePlugin = require('replace-in-file-webpack-plugin')
const StylelintWebpackPlugin = require('stylelint-webpack-plugin')
const packageJson = require('./package.json')
const { app } = require('./config/app')

module.exports = {
  assetsDir: '',
  publicPath: process.env.NODE_ENV === 'production' ? '/' + process.env.CI_PROJECT_NAME + '/' : '/',
  lintOnSave: process.env.NODE_ENV !== 'production' ? 'error' : false,
  crossorigin: 'use-credentials',
  integrity: true,
  chainWebpack(config) {
    config.set('name', app.name)

    config.plugin('replace-in-file').use(ReplaceInFilePlugin, [
      [
        {
          dir: 'dist',
          files: [
            'humans.txt',
            'index.html',
            'manifest.json',
            'robots.txt',
            'sw.js'
          ],
          rules: [
            {
              search: /%APP_NAME%/,
              replace: app.name
            },
            {
              search: /%APP_SHORT_NAME%/,
              replace: app.short_name
            },
            {
              search: /%APP_DOMAIN%/,
              replace: app.domain
            }
          ]
        }
      ]
    ])

    config.plugin('stylelint').use(StylelintWebpackPlugin, [
      {
        files: 'src/**/*.{vue,css,scss}'
      }
    ])

    // if (process.env.NODE_ENV === 'production') {
    //   config.plugin('sentry').use(SentryWebpackPlugin)
    // }

    config.module
      .rule('graphql')
      .test(/\.(graphql|gql)$/)
      .exclude
      .add(/node_modules/)
      .end()
      .use('graphql-tag')
      .loader('graphql-tag/loader')
  },
  pwa: {
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: 'public/sw.js'
    },
    assetsVersion: packageJson.version
  }
}
