module.exports = {
  '*.{js,ts,vue}': ['vue-cli-service lint', 'git add'],
  '*.{css,scss}': ['yarn lint:stylelint', 'git add'],
  '*.{png,jpeg,jpg,gif,svg}': ['imagemin-lint-staged', 'git add'],
  '*.{md}': ['yarn lint:markdown', 'git add']
}
