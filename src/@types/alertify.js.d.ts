// Type definitions for Alertify.js v1.0.12
// Project: https://github.com/alertifyjs/alertify.js
// Definitions by: KasaiDot <https://github.com/KasaiDot>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

declare module 'alertify.js' {
  type _IAlertifyItemType = 'alert' | 'confirm' | 'prompt'

  interface _IAlertifyItem {
    type: _IAlertifyItemType
    message: string
    onOkay?(value?: string, ev?: EventListenerOrEventListenerObject): any
    onOkay?(ev?: EventListenerOrEventListenerObject): any
    onCancel?(ev?: EventListenerOrEventListenerObject): any
    isValid?(value: string): boolean
  }

  interface _IAlertifyDialogsButtons {
    holder: string
    ok: string
    cancel: string
  }

  interface _IAlertifyDialogs {
    buttons: _IAlertifyDialogsButtons
    input: string
    message: string
    log: string
  }

  /**
   * Alertify private object
   */
  interface _IAlertify {
    parent: HTMLElement
    version: string
    defaultOkLabel: string
    okLabel: string
    defaultCancelLabel: string
    cancelLabel: string
    defaultMaxLogItems: number
    maxLogItems: number
    promptValue: string
    promptPlaceholder: string
    closeLogOnClick: boolean
    closeLogOnClickDefault: boolean
    delay: number
    defaultDelay: number
    logContainerClass: string
    logContainerDefaultClass: string
    dialogs: _IAlertifyDialogs
    defaultDialogs: _IAlertifyDialogs

    /**
     * Build the proper message box
     * @param item Current object in the queue
     * @returns An HTML string of the message box
     */
    build(item: _IAlertifyItem): string
    setCloseLogOnClick(bool: boolean): void

    /**
     * Close the log messages
     * @param elem HTML Element of log message to close
     * @param wait [optional] Time (in ms) to wait before automatically hiding the message, if 0 never hide
     */
    close(elem: HTMLElement, wait?: number): void

    /**
     * Create a dialog box
     * @param message The message passed from the callee
     * @param type Type of dialog to create
     * @param onOkay [Optional] Callback function when clicked okay.
     * @param onCancel [Optional] Callback function when cancelled.
     * @param isValid [Optional] Validate function for prompts
     */
    dialog(message: string, type: _IAlertifyItemType, onOkay?: Function, onCancel?: Function, isValid?: Function): any

    /**
     * Show a new log message box
     * @param message The message passed from the callee
     * @param type [Optional] Optional type of log message
     * @param click [Optional] Time (in ms) to wait before auto-hiding the log
     */
    log(message: string, type?: string, click?: number): void
    setLogPosition(str: string): void
    setupLogContainer(): Element

    /**
     * Add new log message
     * If a type is passed, a class name "{type}" will get added.
     * This allows for custom look and feel for various types of notifications.
     * @param message The message passed from the callee
     * @param type [Optional] Type of log message
     * @param click [Optional] Time (in ms) to wait before auto-hiding
     */
    notify(message: string, type?: string, click?: number): void

    /**
     * Initiate all the required pieces for the dialog box
     * @param item ?
     */
    setup(item: _IAlertifyItem): Promise<any>
    okBtn(label: string): _IAlertify
    setDelay(time: number): _IAlertify
    cancelBtn(str: string): _IAlertify
    setMaxLogItems(num: number): void
    theme(themeStr: string): void
    reset(): void
    injectCSS(): void
    removeCSS(): void
  }

  interface IAlertify {
    _$$alertify: _IAlertify
    parent(prt: HTMLElement): void
    reset(): IAlertify
    alert(message: string, onOkay?: Function, onCancel?: Function): IAlertify
    confirm(message: string, onOkay?: Function, onCancel?: Function): IAlertify
    prompt(message: string, onOkay?: Function, onCancel?: Function, isValid?: Function): IAlertify
    log(message: string, click?: Function): IAlertify
    theme(themeName: string): IAlertify
    success(message: string, click?: Function): IAlertify
    error(message: string, click?: Function): IAlertify
    cancelBtn(label: string): IAlertify
    okBtn(label: string): IAlertify
    delay(time: Number): IAlertify
    placeholder(str: string): IAlertify
    defaultValue(str: string): IAlertify
    maxLogItems(max: Number): IAlertify
    closeLogOnClick(bool: Boolean): IAlertify
    logPosition(position: string): IAlertify
    setLogTemplate(template: string): IAlertify
    clearLogs(): IAlertify
    version: string
  }

  const alertify: IAlertify

  export default alertify
}
