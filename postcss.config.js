const purgecss = require('@fullhuman/postcss-purgecss')({
  content: [
    './src/**/*.html',
    './src/**/*.vue',
    './src/**/*.ts',
    './src/**/*.js'
  ],
  defaultExtractor: content => content.match(/[\w-/:%]*(?<!:)/g) || []
})

module.exports = {
  plugins: [
    require('tailwindcss'),
    // require('stylelint'),
    require('autoprefixer'),
    ...(process.env.NODE_ENV === 'production' ? [purgecss] : [])
  ]
}
