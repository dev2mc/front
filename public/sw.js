/* global workbox */
workbox.core.setCacheNameDetails({ prefix: '%APP_NAME%' })

self.__precacheManifest = [].concat(self.__precacheManifest || [])
// workbox.precaching.suppressWarnings()
workbox.precaching.precacheAndRoute(self.__precacheManifest, {})

// install new service worker when ok, then reload page.
self.addEventListener('message', msg => {
  if (msg.data.action === 'skipWaiting') self.skipWaiting()
})

self.addEventListener('activate', () => {
  self.clients
    .matchAll({
      type: 'window'
    })
    .then(clients => {
      for (let client of clients) {
        client.navigate(client.url)
      }
    })
})

self.addEventListener('install', () => {
  self.skipWaiting()
})

self.addEventListener('fetch', e => {
  e.respondWith(
    caches.match(e.request).then(res => {
      return res || fetch(e.request)
    })
  )
})
