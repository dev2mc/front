// Type definitions for register-service-worker v1.6.2
// Project: https://github.com/yyx990803/register-service-worker
// Definitions by: KasaiDot <https://github.com/KasaiDot>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

declare module 'register-service-worker' {
  export type Hooks = {
    registrationOptions?: RegistrationOptions
    ready?: (registration: ServiceWorkerRegistration) => void
    registered?: (registration: ServiceWorkerRegistration) => void
    cached?: (registration: ServiceWorkerRegistration) => void
    updated?: (registration: ServiceWorkerRegistration) => void
    updatefound?: (registration: ServiceWorkerRegistration) => void
    offline?: () => void
    error?: (error: Error) => void
  }

  export function register(swUrl: string, hooks?: Hooks): void

  export function unregister(): void
}
