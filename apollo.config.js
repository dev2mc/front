module.exports = {
  client: {
    service: {
      name: 'localhost',
      // URL to the GraphQL API
      url: 'http://localhost:3000/graphql',
      // optional headers
      headers: {}
    },
    // Files processed by the extension
    includes: [
      'src/**/*.vue',
      'src/**/*.ts'
    ]
  }
}
