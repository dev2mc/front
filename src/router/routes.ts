import Home from '@/views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/tutoriels',
    name: 'tutoriels',
    component: () => import(/* webpackChunkName: "about" */ '@/views/Tutoriels.vue')
  },
  {
    path: '/server-jar',
    name: 'server-jar',
    component: () => import(/* webpackChunkName: "about" */ '@/views/ServerJar.vue')
  },
  {
    path: '/support',
    name: 'support',
    component: () => import(/* webpackChunkName: "about" */ '@/views/Support.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "about" */ '@/views/Login.vue')
  },
  {
    path: '/register',
    name: 'register',
    component: () => import(/* webpackChunkName: "about" */ '@/views/Register.vue')
  }
]

export default routes
