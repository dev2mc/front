module.exports = {
  extends: [
    'stylelint-config-recommended',
    'stylelint-config-standard'
  ],
  rules: {
    'at-rule-no-unknown': [true, {
      ignoreAtRules: [
        'tailwind',
        'apply',
        'variants',
        'responsive',
        'screen'
      ]
    }],
    'declaration-block-trailing-semicolon': null,
    'no-descending-specificity': null,
    'comment-empty-line-before': null,
    'at-rule-empty-line-before': null
  }
}
